#include "io.h"

int foo(int a) {
  {
    int a;
    a = 20;
    return a; 
  }
  return a;
}

void main() {
  int a; char b;
  a = 100;
  b = 'k';
  {
    char a; int b;
    a = 'i'; b = 200;
    print_i(b);
    print_c(a);
  }
  print_i(a);
  print_c(b);
  {
    int a;
    a = 50;
    print_i(a);
  }
  print_i(a);
  a = foo(a);
  print_i(a);
}
                                                                                