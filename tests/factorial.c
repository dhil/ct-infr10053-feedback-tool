#include "print_number.h"

int factorial(int n) {
  int nminus1;
  if(n <= 1) {
    return 1;
  } else {
    nminus1 = n - 1;
    return n * factorial(nminus1);
  }
}

main() {
  int n;
  int result;
  n = 5;
  print_number(n);
  print(" factorial is ");
  result = factorial(n);
  print_number(result);
  print(".\n");
  return 0;
}
