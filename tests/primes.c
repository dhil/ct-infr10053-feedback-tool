#include "print.h"


// Tests whether a number, n, is prime.
int isPrime(int n) {
	int i;
	i = 2;
	while (i < n) {
		if (n % i == 0) {
			return 0;
		}
		i = i + 1;
	}
	return 1;
}

// prints the primes less than n.
int printPrimes(int n) {
	int i;
	i = 2;
	while (i <= n) {
		if (isPrime(i)) {
			output(i);
			print(" is prime.\n");
		}
		i = i+1;
	}
	return 0;
}

/* Comment cos I need a comment */
void main() {
	int PRIME_UNTIL;
	PRIME_UNTIL = 8000;
	printPrimes(PRIME_UNTIL);
}
