/* Call a function with constant parameters */

int myfun(char c, int n) { return 1; }

void main() {
 char c;
 c = 'Y';
 myfun(c, 1); // 1 is a constant
}
                                                                                       