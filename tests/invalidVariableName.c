#include "String.h"
#include "stdio.h"

int 2x; //This is not a valid variable name, as the first character must be numeric.
int yVar;
char aChar0123456789;
int Z0_The_Variable1;
char B_0987654321;
char C;
char newline;

doNothing(){}
doNothingWithInt(int arg1){}
doNothingWithChar(char charArg){}
doNothingWithArgs(int arg1, char arg2, int arg3, char arg4){}

int add(int a, int b){
	return a+b;
}

int increment(int a){
	a = a+1;
	return a;
}

char giveBack(char c){
	return c;
}

displayIntAndChar(int i, char c){
	output(i);
	outputc(c);
	return;
}

int doMaths(){
	int a;
	int b;
	int c;

	char n;

	a = 1; b = 2; c = 3;

	a = a * b * c;
	b = a % b % c;
	c = a / b / c;

	a = a + b + c;
	b = a - b - c;

	return a * b / c % a + b - c;
}


main(){
	x = 0;
	newline = '\n';
	while(x < 10){
		x = increment(x);
		if(x == 1){
			print("Start\n");
		}else if(x == 10){
			print("End\n");
		}else{
			if(x < 5){print("First Half ");}
			if(x >= 5){print("Second Half ");}
			if(x != 5){print("Not 5 ");}
			if(x > 5){print("Large");}
			output(x);
			outputc(newline);
		}
	}

	print("Enter a character:\n");
	readc(C);

	print("Enter an integer:\n");
	read(yVar);

	Z0_The_Variable1 = (x + 500 - 20) / ((yVar * x ) + (x % yVar));

	doNothing();
	doNothingWithInt(x);
	doNothingWithChar(B_0987654321);
	doNothingWithArgs(x, C, yVar, B_0987654321);
	yVar = add(yVar, x);
	displayIntAndChar(yVar, C);
}
