#include "io.h"

int brackets(int a, int b, int c, int d){
  int calculation;
  calculation = (a+b*c+c)*c*b%a+(a+(b+c));
  return calculation;
}
int addDigits(int n){
  /*note we cannot mix declarations
    and assignments - this is desired
    behavious of the grammar */
   int t;
   int remainder;
   int sum;
   
   sum = 0;
   t = n;
   
   while (t != 0)
   {
      remainder = t % 10;
      sum       = sum + remainder;
      t         = t / 10;
   }
   return sum;
}

int isVowel(char ch){
  if (ch == 'a'){
    print_s("This is a vowel.");
	return 1;
  }
  else {
    print_s("This is not a vowel.");
	return 0;
  }
}
void main()
{
  char ch;
  char sum;
  int n;
  n = 148;
  isVowel(ch);
  sum = addDigits(n);
  print_s("Sum of digits of ");
  print_i(n);
  print_s(" is ");
  print_i(sum);
  
  
 
  return;
}
