#include "stdio.h"

int leap_year(int year){
  if (year%400 == 0){
    print("This is a leap year.");
  }
  else if ( year%100 == 0){
    print("This is not a leap year.");
  }
  else if( year%4 == 0 ){
    print("This is a leap year.");
  }
  else{
    print("This is not a leap year.");
  }
}

main()
{
  int year;
  year = 2015;
  leap_year(year);
  return 0;
}