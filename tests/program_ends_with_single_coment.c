/**
 * This program contains no linebreaks after the comment on the final line.
 * The scanner should recognize, and ignore the final comment even though it has no linebreak following it.
 */
void main() {
} // A line break occurs after this comment 
// This is the final line.
                                                                                       