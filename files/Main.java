import ast.ASTPrinter;
import ast.Program;
import lexer.Scanner;
import lexer.Token;
import lexer.Tokeniser;
import parser.Parser;
import sem.SemanticAnalyzer;
import gen.CodeGenerator;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author cdubach
 */
public class Main {
    public static int FILE_NOT_FOUND = 2;
    public static int MODE_FAIL      = 254;
    public static int LEXER_FAIL     = 250;
    public static int PARSER_FAIL    = 245;
    public static int SEM_FAIL       = 240;
    public static int PASS           = 0;
    public static PrintStream stdout = System.out, stderr = System.err;
    
    private enum Mode {
        LEXER, PARSER, AST, SEMANTICANALYSIS, GEN
    }

    public static void usage() {
        System.out.println("Usage: java "+Main.class.getSimpleName()+" pass inputfile outputfile");
        System.out.println("where pass is either: -lexer or -parser");
        System.exit(-1);
    }

    public static void main(String[] args) {

        if (args.length != 3)
            usage();

        Mode mode = null;
        switch (args[0]) {
            case "-lexer":
                mode = Mode.LEXER;
                break;
            case "-parser":
                mode = Mode.PARSER;
                break;
	    case "-ast":
		mode = Mode.AST;
		break;
            case "-sem":
	        mode = Mode.SEMANTICANALYSIS;
		break;
   	    case "-gen":
		mode = Mode.GEN;
		break;
            default:
                usage();
                break;
        }

	// Redirect all output
	redirectOutput();

	
        File inputFile = new File(args[1]);
        File outputFile = new File(args[2]);

        Scanner scanner;
        try {
            scanner = new Scanner(inputFile);
        } catch (FileNotFoundException e) {
            System.out.println("File "+inputFile.toString()+" does not exist.");
            System.exit(-1);
            return;
        }

        Tokeniser tokeniser = new Tokeniser(scanner);
        if (mode == Mode.LEXER) {
	    // Restore original stdout
	    restore();
            for (Token t = tokeniser.nextToken(); t.tokenClass != Token.TokenClass.EOF; t = tokeniser.nextToken()) 
		System.out.println(t);
	    System.exit(tokeniser.getErrorCount() == 0 ? PASS : LEXER_FAIL);
        } else if (mode == Mode.PARSER) {
	    Parser parser = new Parser(tokeniser);
	    parser.parse();
	    System.exit(parser.getErrorCount() == 0 ? PASS : PARSER_FAIL);
        } else if (mode == Mode.AST) {
	    Parser parser = new Parser(tokeniser);
	    Program programAst = parser.parse();
	    if (parser.getErrorCount() == 0) {
		//System.out.println("Parsing: pass");
		//System.out.println("Writing AST into output file");
		PrintWriter writer;
		StringWriter sw = new StringWriter();
		try {
		    writer = new PrintWriter(sw);		
		    programAst.accept(new ASTPrinter(writer));
		    writer.flush();
		    // Restore original stdout
		    restore();
		    System.out.print(sw.toString());
		    writer.close();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	    System.exit(parser.getErrorCount() == 0 ? PASS : PARSER_FAIL);
         } else if (mode == Mode.SEMANTICANALYSIS) {
            Parser parser = new Parser(tokeniser);
            Program programAst = parser.parse();
            if (parser.getErrorCount() == 0) {
          	SemanticAnalyzer sem = new SemanticAnalyzer();
        	int errors = sem.analyze(programAst);
        	System.exit(errors == 0 ? PASS : SEM_FAIL);
            } else
        	System.exit(PARSER_FAIL);
	} else if (mode == Mode.GEN) {
	    Parser parser = new Parser(tokeniser);
            Program programAst = parser.parse();
            if (parser.getErrorCount() == 0) {
          	SemanticAnalyzer sem = new SemanticAnalyzer();
        	int errors = sem.analyze(programAst);
        	if (errors == 0) {
		    CodeGenerator codegen = new CodeGenerator();
		    codegen.emitProgram(programAst);
		    System.exit(0);
		} else {
		    System.exit(SEM_FAIL);
		}
            } else
        	System.exit(PARSER_FAIL);
	} else {
	    System.exit(MODE_FAIL);
	}
    }

    static void redirectOutput() {
	// Fool proof the marking system by redirecting stdout and -err
	//PrintStream stdout = System.out;
	//PrintStream stderr = System.err;
	PrintStream nil = new PrintStream(new OutputStream() {
                public void write(int b) {
                    // Simulates /dev/null by writing nothing.
                }
	    });
	System.setOut(nil);
	System.setErr(nil);
    }

    static void restore() {
	System.setOut(stdout);
	System.setErr(stderr);
    }
}
