#!/usr/bin/python3
#
# Generates the CT scoreboard
#
#

from ctutils import load_config
import os
import json

# Load the configuration
config = load_config()

student_results = None
# Load the testsuite
with open(config['files']['results'], 'r') as suite:
    student_results = json.load(suite)

def student_comparer(s1, s2):
    if s1['uun'] < s2['uun']:
       return -1
    elif s1['uun'] == s2['uun']:
       return 0
    else:
        return 1
    
def qsort(mylist, compare):
    less = []
    equal = []
    greater = []

    if len(mylist) > 1:
        pivot = mylist[0]
        for x in mylist:
            if compare(x, pivot) < 0:
                less.append(x)
            elif compare(x, pivot) == 0:
                equal.append(x)
            else:
                greater.append(x)
        # Don't forget to return something!
        return qsort(less, compare)+equal+qsort(greater, compare) # Just use the + operator to join lists
    else: 
        return mylist

    
def group_results(results):
    component_idx = 4
    part1 = list(filter(lambda r: r[component_idx] == "lexer" or r[component_idx] == "parser", results))
    part2 = list(filter(lambda r: r[component_idx] == "ast", results))
    part3 = list(filter(lambda r: r[component_idx] == "sem", results))
    part4 = list(filter(lambda r: r[component_idx] == "gen", results))
    return {'part1': part1, 'part2': part2, 'part3': part3, 'part4': part4}

def massage_results_format(results):
    return list(map(lambda r: {'testname': r[0], 'result': r[1], 'exit_code': r[2], 'expected_exit_code': r[3], 'component': r[4]}, results))
    
def has_passed(result):
    value = result[1]
    if value == 0:
        return True
    else:
        return False

def compute_score_part(part):
    return len(list(filter(lambda r: has_passed(r), part)))  
    
def compute_score(student):
    parts        = student['results']
    part1_score  = compute_score_part(parts['part1'])
    part2_score  = compute_score_part(parts['part2'])
    part3_score  = compute_score_part(parts['part3'])
    part4_score  = compute_score_part(parts['part4'])
    student.update({'part1_score': part1_score, 'part2_score': part2_score, 'part3_score': part3_score, 'part4_score': part4_score})

    total   = sum(list(map(lambda p: len(p), [parts['part1'],parts['part2'],parts['part3'],parts['part4']])))
    student.update({'passed': sum([part1_score,part2_score,part3_score,part4_score]), 'total': total, 'success': total > 0})
    
    if total == 0:
        if student['cloned'] == False:
            student.update({'failure_reason': "Could not clone your repository."})
        elif student['compiles'] == False:
            student.update({'failure_reason': "Could not build your compiler."})
        else:
            student.update({'failure_reason': "Something went wrong. Contact the teaching staff."})
    return student

def normalize_student(student):
    student.update({'alias': student['username']})
    if not('results' in student):
        student.update({'results':[]})
    student.update({'results': group_results(student['results'])})
    return student

def interpret_result(result):
    if result == 0:
        return "Pass"
    elif result > 0:
        return "Fail"
    else:
        return "An error occurred"

def get_result_class(result):
    if result == 0:
        return "alert-success"
    elif result > 0:
        return "alert-danger"
    else:
        return "alert-warning"
    
def make_info_row(row):
    return "<td>" + str(row[0]) + "</td><td>" + str(row[1]) + "</td>"
            
def make_overall_row(row):
    alias = str(row['alias'])
    return "<td><a href=\"" + alias + ".html\">" + alias + "</a></td><td style=\"text-align:center;\">" + str(row['passed']) + " / " + str(row['total']) + "</td>"

def make_unsuccessful_row(row):
    alias = str(row['alias'])
    return "<td><a href=\"" + alias + ".html\">" + alias + "</a></td><td style=\"text-align:center;\">" + row['failure_reason'] + "</td>"

def make_unregistered_row(row):
    alias = str(row['alias'])
    return "<td>" + alias + "</td>"

def make_part_row(row):
    resultcls = get_result_class(row['result'])
    return "<td class=\"" + resultcls + "\">" + str(row['testname']) + "</td><td class=\"" + resultcls + "\">" + str(row['component']) + "</td><td class=\"" + resultcls + "\">" + str(row['exit_code']) + "</td><td class=\"" + resultcls + "\">" + str(row['expected_exit_code']) + "</td><td class=\"" + resultcls + "\">" + interpret_result(row['result']).capitalize() + "</td>"

def make_table(rows, make_row, headers = []):
    t = "<table border=\"1\" cellspacing=\"1\" cellpadding=\"5\">"
    if len(headers) > 0:
        t = t + "<tr>" + "".join(str(x) for x in map(lambda h: "<td class=\"hd\">" + str(h) + "</td>", headers)) + "</tr>"
    t = t + "".join(str(x) for x in list(map(lambda r: "<tr>" + str(make_row(r)) + "</tr>", rows))) + "</table>"    
    return t

# Extract results
registered_students = list(map(compute_score, list(filter(lambda s: s['is_registered'], list(map(normalize_student, student_results))))))
registered_students = qsort(registered_students, student_comparer)
unregistered_students = list(filter(lambda s: s['is_registered'] == False, student_results))


# Generate main table
with open(os.path.join(config['directories']['scoreboard'], "scoreboard.html"), 'w') as f:
    successful = list(filter(lambda s: s['success'] == True, registered_students))
    unsuccessful = list(map(lambda r: {'alias': r['username'], 'failure_reason': r['failure_reason']}, list(filter(lambda s: s['success'] == False, registered_students))))
    overall = list(map(lambda r: {'alias': r['username'], 'passed': r['passed'], 'total': r['total']}, successful))
    
    quickinfo = [("Successful builds", len(successful)), ("Unsuccessful builds", len(unsuccessful)), ("Unregistered", len(unregistered_students))]
    
    f.write("<!DOCTYPE html><html><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><title>CT Scoreboard</title>")
    f.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\"><style type=\"text/css\">body { margin: 0 auto; width: 50%; padding-bottom: 30px; } td.hd { font-weight: bold; text-align: center; } td { padding: 5px; }</style></head>")
    f.write("<body>")
    f.write("<div class=\"container\">")
    f.write("<h2 class=\"page-header\">CT Scoreboard</h2>")

    f.write(make_table(quickinfo, make_info_row))
                 
    if len(successful) > 0:
        f.write("<h3 class=\"page-header\">Successful builds</h3>")
        f.write(make_table(overall, make_overall_row, ["Student", "Passed / Total"]))

    if len(unsuccessful) > 0:
        f.write("<h3 class=\"page-header\">Unsuccessful builds</h3>")
        f.write(make_table(unsuccessful, make_unsuccessful_row, ["Student", "Reason"]))

    if len(unregistered_students) > 0:
        f.write("<h3 class=\"page-header\">Unregistered</h3>")
        f.write(make_table(unregistered_students, make_unregistered_row, ["Username"]))
        
    f.write("</div>")
    f.write("</body>")
    f.write("</html>")
    f.flush()

def display_score(student, part):
    tests = student['results'][part]
    return str(compute_score_part(tests)) + " / " + str(len(tests))
    
# Generate individual pages
for student in registered_students:
    alias = student['username']
    results = student['results']
    scoretable = [("<b>Part</b>", "<b>Passed / Total</b>"), ("Part 1", display_score(student, 'part1')), ("Part 2", display_score(student, 'part2')), ("Part 3", display_score(student, 'part3')), ("Part 4", display_score(student, 'part4'))]
    ecodetable = [("<b>Code</b>", "<b>Explanation</b>"), ("250", "One or more lexer error(s)."), ("245", "One or more parse error(s)."), ("240", "One or more semantic error(s)."), ("230", "Compiled program produced wrong output."), ("224","AST construction is wrong."), ("204", "Compiled program not found."), ("124", "Your compiler or produced program might have an infinite loop."), ("1", "JVM exited prematurely. It occurs when your compiler has infinite recursion, requires a Java version different from 1.7, etc.")]
    infotable = [("Alias", alias), ("Start", student['marking_begun']), ("End", student['marking_ended']), ("Comment ID", student['commitid']), ("Scores", make_table(scoretable, make_info_row)), ("Exit codes", make_table(ecodetable, make_info_row))]        

    with open(os.path.join(config['directories']['scoreboard'], alias + ".html"), 'w') as f:
        f.write("<!DOCTYPE html><html><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">")
        f.write("<title>CT Scoreboard for %s</title>" % (alias))
        f.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\"><style type=\"text/css\">body { margin: 0 auto; width: 50%; padding-bottom: 30px; } td.hd { font-weight: bold; text-align: center; } td { padding: 5px; }</style></head>")

        f.write("<body><div class=\"container\">")
        f.write("<h2>Information</h2>\n")

        f.write("<div class=\"container\" style=\"width: 100%;\">")
        f.write(make_table(infotable, make_info_row))

        if student['success']:
            result_headers = ["Test name", "Component", "Exit code", "Expected exit code", "Result"]
            if len(results['part4']) > 0:
                f.write("<h2 class=\"page-header\">Code generation <small>(Part 4)</small></h2>")
                f.write("<div class=\"container\" style=\"width: 75%;margin-top:20px;\">")
                f.write(make_table(massage_results_format(results['part4']), make_part_row, result_headers))
                f.write("</div>")
                
            if len(results['part3']) > 0:
                f.write("<h2 class=\"page-header\">Semantic analysis <small>(Part 3)</small></h2>")
                f.write("<div class=\"container\" style=\"width: 75%;margin-top:20px;\">")
                f.write(make_table(massage_results_format(results['part3']), make_part_row, result_headers))
                f.write("</div>")
                
            if len(results['part2']) > 0:
                f.write("<h2 class=\"page-header\">AST construction <small>(Part 2)</small></h2>")
                f.write("<div class=\"container\" style=\"width: 75%;margin-top:20px;\">")
                f.write(make_table(massage_results_format(results['part2']), make_part_row, result_headers))
                f.write("</div>")            
                
            if len(results['part1']) > 0:
                f.write("<h2 class=\"page-header\">Lexer &amp; parser <small>(Part 1)</small></h2>")
                f.write("<div class=\"container\" style=\"width: 75%;margin-top:20px;\">")
                f.write(make_table(massage_results_format(results['part1']), make_part_row, result_headers))
                f.write("</div>")
        else:
            f.write("<div class=\"container\" style=\"width: 45%;margin-top:30px;\"><div class=\"alert alert-danger\" role=\"alert\"><b>Test suite failure!</b> " + student['failure_reason'] + "</div></div>")
            f.write("<h3>Error log</h3><pre>")
            f.write("".join(str(x) for x in list(student['errorlog'])))
            f.write("</pre></fieldset>")
        f.write("</div>")
                                  
        f.write("<hr /><div class=\"container\" style=\"width:75%;\"><a href=\"scoreboard.html\"><button type=\"button\" class=\"btn btn-default\">Go back</button></a></div>")
        f.write("</div></body></html>")
        f.flush()
