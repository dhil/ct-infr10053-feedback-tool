#!/usr/bin/python3
#
# Dependencies: requests, request_oauthlib
#
import requests
from requests_oauthlib import OAuth1
import os
from ctutils import load_config

class BBClient(object):
  __auth = None
  __repo = ""
  __config = None
  def __init__ (self):
    self.__config = load_config()
    config = self.__config
    self.__auth = OAuth1(config['api']['secret'], config['api']['key'])
    # Note: The below join-method is probably not portable
    self.__repo = os.path.join(config['api']['href'], config['repository']['owner'], config['repository']['slug'])
    
  # Returns a list forks of owner/repo_name
  # The returned list consists of pairs, where
  # a pair has the form (username, clone-link (ssh), last update timestamp)
  def get_forks(self):
    myfilter = lambda fork: True
    myprojection = lambda fork: (fork['owner']['username'], fork['links']['clone'][1]['href'], fork['updated_on'], fork['full_name'])
    return self.__get_forks(select = myfilter, project = myprojection)

  # Retrieves a list of public forks of the repository
  def get_public_forks(self):
    myfilter = lambda fork: fork['is_private'] == False
    myprojection = lambda fork: (fork['owner']['username'], fork['owner']['display_name'], fork['links']['clone'][1]['href'])
    return self.__get_forks(select = myfilter, project = myprojection)

  # Retrieves a list of forks of the repository
  # The function select is a filter applied to the list, before it is returned.
  # The function project is a projection applied to each element in the list before it is returned.
  def __get_forks(self, select = (lambda x: x), project = (lambda x: x)):
    payload = {'pagelen': '100'}
    r = requests.get(os.path.join(self.__repo, 'forks'), auth=self.__auth, params=payload)
    forks = r.json()
    return map(project, filter(select, forks['values']))

  def get_latest_commit_id(self, repository):
    payload = {'pagelen': 1}
    r = requests.get(os.path.join(self.__config['api']['href'], os.path.join(repository, 'commits')), auth=self.__auth, params=payload)
    commit = r.json()
    return commit['values'][0]['hash']
    
