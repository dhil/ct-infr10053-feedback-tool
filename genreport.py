#!/usr/bin/python3
# 
# Automated marking script for "Compiling Techniques (UG3)" course
# at the University of Edinburgh.
# 
# Written by Daniel Hillerström (daniel.hillerstrom@ed.ac.uk)
# during a September afternoon (2015-09-25)
#
import os, tempfile, shutil
from subprocess import call, Popen, PIPE
import datetime, json
from math import floor
from ctutils import load_config, compute_hash
import subprocess, shlex
from threading import Timer

def run(cmd, timeout = 10, redirect_to = "/dev/stdout"):
    timeout_cmd = []
    if timeout <= 0:
        timeout_cmd = cmd
    else:
        timeout_cmd = ["timeout", str(timeout) + "s"] + cmd
    exit_code = -1
    with open(redirect_to, 'w') as outfile:
        exit_code = call(timeout_cmd, stdout=outfile, stderr=outfile)
    return exit_code

def input_run(inp, cmd, timeout = 10, redirect_to = "/dev/stdout"):
    with open(redirect_to, 'w') as outfile:
        p1 = Popen(["echo", "-e", str(inp)], stdout=PIPE)
        p2 = Popen(["timeout", str(timeout) + "s"] + cmd, stdin=p1.stdout, stdout=outfile, stderr=outfile)
        output = p2.communicate()
        return p2.returncode
    
#
# Load config and test suite into memory
#
config     = load_config()
testsuite  = None

SUBMISSIONDIR = os.getcwd()

# Load the testsuite
with open(config['files']['testsuite'], 'r') as suite:
    testsuite = json.load(suite)

# Generates a timestamp
def now(format = '%Y-%m-%d %H:%M:%S'):
    return datetime.datetime.now().strftime(format)

# Puts text file contents into a string
def read_file(filename):
    with open(filename) as f:
        content = f.readlines()
    return content

# Trims all whitespace, and capitalises every character
def trim_and_uppercase(s):
    s = "".join(str(x) for x in list(s))
    return s.replace(" ", "").replace("\t","").replace("\n","").upper()

#
# Idea: The marking process is separated into individual, specialised components that are composed
# to make a pipeline.
#
def run_pipeline(source, pipeline):
    for step in pipeline:
        source = step(source)
    return source

# Pipes
def process(forks, pipeline):
    # Set-up temporary working directory
    pwd = os.getcwd()
    tmpdir = tempfile.mkdtemp()
    os.chdir(tmpdir)

    # Divide the work
    # Run pipeline
    results = run_pipeline(forks, pipeline)
        
    # Clean-up, remove the temporary working directory
    os.chdir(pwd)
    shutil.rmtree(tmpdir)
    return results

def transform_fork_data(forks):
    for fork in forks:
        fork.update({'marking_begun': now(), 'is_registered': True})
        yield fork
    
def clone_and_checkout(projects):
    for project in projects:        
        username = project['username']
        submission = os.path.join(SUBMISSIONDIR, project['uun'])
        target = os.path.join(os.getcwd(), username)
        if project['is_registered'] == True:
            clone_cmd = ["cp", "-r", submission, target] # Generates: cp -r UUN STUDENT_USERNAME
            print(str(clone_cmd))
            exit_code = run(clone_cmd, timeout = 0, redirect_to = "/dev/stdout")
            project.update({'cloned': True, 'project_directory': target})
            
            pwd = os.getcwd()
            os.chdir(target)
            yield project
            os.chdir(pwd)
        else:
            project.update({'cloned':False})
            yield project

def check_main_file(projects):
    for project in projects:
        if project['cloned']:
            dest = os.path.join(project['project_directory'], os.path.join('src', config['files']['main']['filename']))
            dest_lib = os.path.join(project['project_directory'], os.path.join('lib', config['files']['lib']['io']))
            dest_asm = os.path.join(project['project_directory'], 'lib/')
            cp_main_cmd = ["cp", config['files']['main']['file'], dest]
            cp_lib = ["cp", config['files']['lib']['io'], dest_lib]
            cp_asm = ["cp", config['files']['lib']['asm-jar'], config['files']['lib']['asm-pom'], dest_asm]
            exit_code = run(cp_main_cmd, timeout = 0, redirect_to = '/dev/null')
            run(cp_lib, timeout = 0, redirect_to = '/dev/null')
            run(cp_asm, timeout = 0, redirect_to = '/dev/null')
            #main_hash = config['files']['main']['hash']
            #main_file = os.path.join(project['project_directory'], os.path.join('src', config['files']['main']['filename']))
            #project.update({'main_hash_verified': os.path.isfile(main_file) and compute_hash(main_file) == main_hash})            
            project.update({'main_hash_verified': exit_code == 0})
        else:
            project.update({'main_hash_verified': False})
        yield project

def compile_the_compiler(projects):
    for project in projects:
#        if project['main_hash_verified'] == True:
            # Copy ANT build file
            cp_build = ["cp", config['files']['buildfile'], "./build.xml"]
            run(cp_build, timeout = 0, redirect_to = "/dev/null")
            # Compile the compiler
            clean_cmd   = ["ant", "clean"]    
            compile_cmd = ["ant", "build"]
            run(clean_cmd, timeout = 0, redirect_to = "/dev/null")
            buildlog = "ct.build.log"
            exit_code = run(compile_cmd, timeout = 0, redirect_to = buildlog)
            project.update({'compiles': exit_code == 0})
            if project['compiles'] == False:
                project.update({'errorlog': read_file(buildlog)})
            else:
                project.update({'errorlog': ""})
            yield project
 #       else:
  #          project.update({'compiles': False})
#        yield project

def run_test(project, test):
    # Compile
    name = test['name']
    inputfile  = test['file']
    outputfile = os.path.join(project['project_directory'], "out/Main" + ".class")
    expected_exit_code = test['exit_code']
    compile_test_cmd = [config['files']['compiler']['runtime'], "-Djava.security.manager", "-Djava.security.policy=" + config['files']['compiler']['policy'], "-cp", "bin/:lib/:lib/asm-all-4.2.jar", "Main", "-" + test['pass'], inputfile, outputfile]
    outlog = os.path.join(project['project_directory'], name + ".log")
    result = 0
    exit_code = 99
    if os.path.isfile(inputfile):
        exit_code = run(compile_test_cmd, timeout = 10, redirect_to = outlog)
        if exit_code == expected_exit_code:
            if test['pass'] == "gen":                
                expected_exit_code = test['prog_exit_code']
                if ('stdout' in test):
                    if os.path.isfile(outputfile):
                        if 'stdin' in test:
                            results = []
                            exit_codes = []
                            n = len(test['stdin'])
                            print(test['name'])
                            for i in range(0, n):
                                inp = "".join(test['stdin'][i])
                                #run_cmd = ["echo", "-e", inp, "|", config['files']['compiler']['runtime'], "-Djava.security.manager", "-Djava.security.policy=" + config['files']['compiler']['policy'], "-cp", "lib/:out/", "Main"]
                                run_cmd = [config['files']['compiler']['runtime'], "-Djava.security.manager", "-Djava.security.policy=" + config['files']['compiler']['policy'], "-cp", "lib/:out/", "Main"]
                                #prog_exit_code = run([" ".join(str(x) for x in run_cmd)], timeout = 5, redirect_to = outlog)
                                prog_exit_code  = input_run(inp, run_cmd, timeout = 5, redirect_to = outlog)
                                exit_codes.append(prog_exit_code)
                                output     = read_file(outlog)
                                output_str = "".join(str(x) for x in output)
                                expected   = "".join(str(x) for x in test['stdout'][i])
                                print(output_str + " == " + expected)
                                if output_str == expected:
                                    results.append(0)
                                else:
                                    results.append(1)
                                    break
                                
                            if all(e == test['prog_exit_code'] for e in exit_codes) and all(r == 0 for r in results):
                                result = 0
                                exit_code = test['prog_exit_code']
                            else:
                                result = 1
                                exit_code = 230 # Produced wrong output
                        else:
                            run_cmd = [config['files']['compiler']['runtime'], "-Djava.security.manager", "-Djava.security.policy=" + config['files']['compiler']['policy'], "-cp", "lib/:out/", "Main"]
                            exit_code = run(run_cmd, timeout = 5, redirect_to = outlog)
                            output = read_file(outlog)
                            output_str = "".join(str(x) for x in output)
                            expected = "".join(str(x) for x in test['stdout'])
                            #print(output_str + " == " + expected)
                            if expected == output_str:
                                if exit_code == expected_exit_code:
                                    result = 0
                                else:
                                    result = 1
                            else:
                                result = 1
                                exit_code = 230
                    else:
                        result = 1
                        exit_code = 204 # Output file not found
                else:
                    run_cmd = [config['files']['compiler']['runtime'], "-Djava.security.manager", "-Djava.security.policy=" + config['files']['compiler']['policy'], "-cp", "lib/:out/", "Main"]
                    exit_code = run(run_cmd, timeout = 5, redirect_to = '/dev/null')
                    if exit_code == expected_exit_code:
                        result = 0
                    else:
                        result = 1
            elif 'stdout' in test:
                # Run program
                #run_cmd = ["java", "-cp", "bin", outputfile]
                #exit_code = run(run_cmd, timeout = 10, redirect_to = outlog)
                # Compare stdout with actual output
                output = trim_and_uppercase(read_file(outlog))
                expected = trim_and_uppercase(test['stdout'])
                if output == expected:
                    result = 0
                else:              
                    result = 1
                    exit_code = 224
            else:
                result = 0 # Interpreted as PASS
        else:
            result = 1 # Interpreted as FAILED
    else:
        result = -100 # Interpreted as the automated marker failed
    return (name, result, exit_code, expected_exit_code, test['pass'])

def __set_test_file_path(settings, test, component):
    test.update({'file': os.path.join(settings['basedir'], test['file']), 'pass': component})
    return test

def run_test_suite(projects):
    settings = testsuite['settings']
    tests = []
    today = now(format = "%Y-%m-%d")
    tests = tests + list(map(lambda t: __set_test_file_path(settings, t, "lexer"), testsuite['tests']['lexer']))
    tests = tests + list(map(lambda t: __set_test_file_path(settings, t, "parser"), testsuite['tests']['parser']))
    tests = tests + list(map(lambda t: __set_test_file_path(settings, t, "ast"), testsuite['tests']['ast']))
    tests = tests + list(map(lambda t: __set_test_file_path(settings, t, "sem"), testsuite['tests']['sem']))
    tests = tests + list(map(lambda t: __set_test_file_path(settings, t, "gen"), testsuite['tests']['gen']))      
        
    for project in projects:
        if project['compiles'] == True:
            project.update({'results': list(map(lambda t: run_test(project, t), tests))})
        yield project

def assign_mark(projects):
    for project in projects:
        if 'results' in project:
            negative_weight = 1
            mark = 100 + sum(list(map(lambda r: -1 * negative_weight if r[1] > 0 else 0, project['results'])))
            project.update({'mark': mark})
        else:
            project.update({'mark':0})
        yield project
        
def timestamp_projects(projects):
    for project in projects:
        project.update({'marking_ended': now()})
        if project['cloned']:
            shutil.rmtree(project['project_directory']) # Removes the project direc
        yield project
        
def collect_results(projects):
    return list(map(lambda x: x, projects)) # Force evaluation by mapping the identity function over the list of projects
        
# Composing the pipes
marking_steps = [
      transform_fork_data
    , clone_and_checkout
    , check_main_file
    , compile_the_compiler
    , run_test_suite
    , assign_mark
    , timestamp_projects
    , collect_results
]

# Preprocess submissions
submissions = [ "s0835905"
    , "s1115104"
    , "s1207068"
    , "s1207825"
    , "s1209562"
    , "s1209563"
    , "s1210443"
    , "s1213677"
    , "s1214019"
    , "s1215551"
    , "s1227500"
    , "s1228405"
    , "s1231350"
    , "s1231564"
    , "s1235260"
    , "s1239548"
    , "s1245356"
    , "s1245947"
    , "s1247438"
    , "s1247578"
    , "s1248784"
    , "s1249355"
    , "s1249759"
    , "s1249982"
    , "s1305395"
    , "s1307304"
    , "s1308009"
    , "s1309082"
    , "s1310496"
    , "s1310540"
    , "s1310787"
    , "s1311898"
    , "s1315613"
    , "s1319624"
    , "s1320852"
    , "s1322776"
    , "s1326224"
    , "s1327953"
    , "s1332298"
    , "s1332702"
    , "s1337523"
    , "s1338564"
    , "s1339895"
    , "s1340038"
    , "s1340597"
    , "s1342227"
    , "s1342377"
    , "s1342823"
    , "s1343145"
    , "s1343872"
    , "s1343880"
    , "s1345559"
    , "s1346707"
    , "s1346981"
    , "s1347555"
    , "s1349219"
    , "s1351669"
    , "s1352745"
    , "s1353184"
    , "s1354255"
    , "s1362882"
    , "s1371464"
    , "s1449654"
    , "s1539660"
    , "s1553002"
    , "s1563190"
    , "s1567300"
    , "s1568062"
    , "s1572438"
    , "s1572678"
    , "s1572730"
    , "s1573919"
    , "s1574377"
    , "s1574635"
    , "s1576709"
    , "s1577363"
    , "s1579535"]
projects = []
for uun in submissions:
     projects.append({"uun": uun, "username": uun, "commitid": "None"})
    
results = process(projects, marking_steps)
# Store the results
print(str(results))
with open(config['files']['results'], 'w') as outfile:
    json.dump(results, outfile, sort_keys = True, indent = 2, separators = (',',':'))
