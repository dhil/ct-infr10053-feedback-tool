#!/usr/bin/python3
#
# Collection of utility functions
#
import json, csv, hashlib

# Private variables
__config = None
__studentsdb = None

def load_config(filename = "./tools/config.json"):
    global __config
    if __config is None:
        with open(filename) as config_json:
            __config = json.load(config_json)
    return __config

def __load_students():
    return None

def get_uun(username):
    config = load_config()
    with open(config['files']['students'], newline='') as csvfile:
        studentreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in studentreader:
            if len(row) >= 2:
                if row[1].lower() == username.lower():
                    return row[0]

    return None

def get_uun_s(username):
    uun = get_uun(username)
    if uun is None:
        return "NOT REGISTERED"
    else:
        return uun

def get_students():
    config = load_config()
    students = []
    with open(config['files']['students'], newline='') as csvfile:
        studentreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in studentreader:
            if len(row) >= 1:
                    students.append(row)

    return students

# Computes the hash of a given file
def compute_hash(file):
    BLOCKSIZE = 4096
    hasher = hashlib.md5()
    with open(file, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()
